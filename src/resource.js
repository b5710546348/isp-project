var res = {
    mainBackground_png : "res/images/Background/Background.png",
    Magician_right : "res/images/Character/Magician-right.png",
    Magician_left : "res/images/Character/Magician-left.png",
    FireBalll : "res/FireBall.png",
    MainMenuMusic : "res/MainScreen-music.mp3",
    GameMusic : "res/Game-music.mp3",
    FireBallSoundEffect : "res/Fireball-sound.mp3",
    PotionConsume : "res/potion_consume.mp3"
    
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}