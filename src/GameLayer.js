var score = 0;
var num_enemy = 0 ;
var hp = 5;
var numberFireball = 0;
var number_potion = 0;
var max_enemy = 5;
var potionExist = false;
var musicIsStart = false;
var GameLayer = cc.LayerColor.extend({
	self : this ,
        init: function() {
		this._super(new cc.Color(127,127,127,255));
		this.setPosition(new cc.Point (0,0));
		if(!musicIsStart){
            cc.audioEngine.playMusic('res/Game-music.mp3', true)
            cc.audioEngine.setMusicVolume(0.3);
            musicIsStart = true;
        }
        
        this.background = new Background();
		this.background.setPosition(screenWidth / 2, screenHeight / 2);
		this.addChild( this.background );
        
        this.lifeLabel = cc.LabelTTF.create("Life : "+hp, 'Arial', 25 );
		this.lifeLabel.setPosition(new cc.Point( 80,screenHeight - 50));
        this.addChild(this.lifeLabel);
        
        this.scorelabel = cc.LabelTTF.create("Score : "+ score , 'Arial', 25 );
		this.scorelabel.setPosition(new cc.Point(80 ,screenHeight - 100));
        this.addChild(this.scorelabel);
        
        this.IceSpike = new IceSpike();
        this.addChild(this.IceSpike);
        this.IceSpike.scheduleUpdate();
            
        this.character = new Character(this,this.IceSpike);
        this.addChild( this.character );
        this.character.scheduleUpdate();
        
        this.enemySet = [];
        this.checkEnemy();
        //this.createSnowflake();
        
        this.addKeyboardHandlers();
		this.scheduleUpdate();
        
	},
    
    update : function () {
        this.attack();
        this.checkEnemy();
        this.updateLifeAndScore();
        this.difficulty();
        this.randomAnchor();
        if(hp >= 0){
            this.RandomPotion();   
        }
        if(hp <= 0){
            cc.audioEngine.stopMusic(res.GameMusic);
            cc.director.runScene(new GameOverScene());   
        }
    },
    
    checkEnemy : function () {
        if(num_enemy <= max_enemy){
            this.createEnemy();   
        }
        
    },
    
    createEnemy : function () {
        for(var i= 0 ; i < 1 ; i++){
            var PosX = (i+1) * 80;
            this.enemy = new Enemy( this , Math.random()*630 + 170);
            this.enemySet.push(this.enemy);
            num_enemy++;
            this.addChild(this.enemy);
        }
    },
    
    RandomPotion : function () {
        var random = Math.floor(Math.random()*1000);
        if(hp <= 3 && (random % 444 == 0) && number_potion < 1){
            this.potion = new Potion();
            this.addChild(this.potion);
            this.potion.scheduleUpdate();
            potionExist = true;
            number_potion++;
        }
        
    },
    
    createSnowflake : function () {
        for(var i = 0 ; i < 35 ; i++){
            this.snowflake = new Snowflake();
            this.snowflake.setPosition(new cc.p((Math.random()*800) , (Math.random() * 600)));
            this.addChild(this.snowflake);
        }
        
    },
    
    attack : function(){
       var pos = this.character.getPosition();
        if(Character.DIR[cc.KEY.space] && !Character.DIR[cc.KEY.left] && !Character.DIR[cc.KEY.right] && 
           numberFireball == 0 ){
            this.fireball = new FireBall(this , this.enemySet);
            this.fireball.setPosition(new cc.p(pos.x + 5 , pos.y + 45));
            this.addChild(this.fireball);
            if(musicIsStart){
                cc.audioEngine.playEffect('res/Fireball-sound.mp3');
                cc.audioEngine.setEffectsVolume(0.3);
            }
            numberFireball++;
        }
    },
    
    addKeyboardHandlers: function() {
      cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                Character.DIR[keyCode] = true;
            },
            onKeyReleased: function( keyCode, event ) {
                Character.DIR[keyCode] = false;
            }
        }, this);
    },
        
    updateLifeAndScore : function (){
        this.lifeLabel.removeFromParent();
        this.lifeLabel = cc.LabelTTF.create("Life : "+hp, 'Arial', 25 );
		this.lifeLabel.setPosition(new cc.Point( 80,screenHeight - 50));
        this.addChild(this.lifeLabel);
        
        this.scorelabel.removeFromParent();
        this.scorelabel = cc.LabelTTF.create("Score : "+ score , 'Arial', 25 );
		this.scorelabel.setPosition(new cc.Point(80 ,screenHeight - 100));
        this.addChild(this.scorelabel);
    },
    
    difficulty : function(){
        if(score%20 == 0){
            max_enemy = score/20 + 5;  
        }
        
    },
    
    randomAnchor : function(){
        if(Math.floor(Math.random()*1000) % 333 == 0 & score > 30){
            this.anchor = new Anchor(this);   
            this.addChild(this.anchor);
        }
        
    },

  
});

var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild(layer);
    }
});
