var Anchor = cc.Sprite.extend({
	ctor:function (gm) {
        this._super();
        this.initWithFile('res/anchor.png');
        this.setPosition(new cc.p(Math.random()*600 + 170 , 660));
        this.GameLayer = gm;
        this.scheduleUpdate();
    },
    
    update : function(){
        this.AnchorMovement();  
        this.isHitCharacter();
    },
    
    AnchorMovement : function (){
        var pos = this.getPosition();
        if(pos.y > -200){
            pos.y -= 15;
            
        }
        this.setPosition(new cc.p(pos.x,pos.y));
    },
    
    isHitCharacter : function(){
        var pos = this.getPosition();
        var charPos = this.GameLayer.character.getPosition();
        if(Math.abs(pos.y - charPos.y) <= 100 && Math.abs(pos.x - charPos.x) <= 50){
            hp--;
            this.removeFromParent();
        }
    }
});