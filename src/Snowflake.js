var Snowflake = cc.Sprite.extend({
	ctor:function () {
        this._super();
        this.initWithFile( 'res/Snowflake.png');
        this.scheduleUpdate();
    },
    
    update : function () {
        this.snowflakeMovement();
    },
    
    snowflakeMovement : function () {
        var pos = this.getPosition();
        if(pos.y > -5){
            pos.y -= 0.2;
            this.setPosition(new cc.p(pos.x , pos.y));    
        }
        else{
            this.setPosition(new cc.p(Math.random()*800 , screenHeight));
        }
    }
});