var Potion = cc.Sprite.extend({
	ctor:function () {
        this._super();
        this.initWithFile('res/potion.png');
        this.setPosition(new cc.p(Math.random()*600 + 170 , screenHeight ));
    },
    
    update : function(){
       this.PotionMovement();
    },
    
    PotionMovement : function(){
        var pos = this.getPosition();
        if(pos.y > -50 ){
            pos.y -= 2;   
        }
        if(pos.y <= -50){
           this.removeFromParent(); 
        }
        this.setPosition(new cc.p(pos.x , pos.y)); 
        
    }
});