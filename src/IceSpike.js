var IceSpike = cc.Sprite.extend({
	ctor:function () {
        this._super();
        this.initWithFile('res/IceSpike.png');
        this.initX = 50;
        this.setPosition(new cc.p(this.initX , -20))
    },
    
    update : function(){
        this.IceSpikeMovement();
    },
    
    IceSpikeMovement : function (){
        var pos = this.getPosition();
        pos.y += 0.2;
        this.setPosition(new cc.p(pos.x , pos.y));
        if(pos.y > 45){
            this.setPosition(new cc.p(Math.random()*(screenWidth-40) + 170 , -20));  
        }
        
    }
});