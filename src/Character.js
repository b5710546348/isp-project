var isEatPotion = false;
var Character = cc.Sprite.extend({
	ctor : function (gamelayer,icespike) {
        this._super();
        this.initWithFile( res.Magician_right);
        this.setPosition(cc.p(screenWidth/2 , 40));
        this.speed = 5;
        this.position = "";
        this.GameLayer = gamelayer;
        this.IceSpike = icespike;
    },
     
    update : function() {
       this.updateMovement();
       this.attackedByIceSpike();
       this.eatPotion();
    },
    
    updateMovement : function () {
        var pos = this.getPosition();
        var self = this;
        var move = false;
        if(Character.DIR[cc.KEY.left]  && pos.x > 170 ){
            pos.x -= this.speed;
            this.initWithFile( res.Magician_left);
            self.setPosition(new cc.Point(pos.x,pos.y));
            this.position = "left";
            numberFireball = 0;
        }
        if(Character.DIR[cc.KEY.right] && pos.x < screenWidth - 30){
            pos.x += this.speed;
            this.initWithFile( res.Magician_right);
            self.setPosition(new cc.Point(pos.x,pos.y));
            this.position = "right";
            numberFireball = 0;
        }
       
        if(pos.y > 40){
            pos.y -= 1;
            this.setPosition(new cc.p(pos.x,pos.y));
        }
    },
    
    attackedByIceSpike : function (){
        var pos = this.getPosition();
        var icespikePos = this.IceSpike.getPosition();
        if(Math.abs(pos.y - icespikePos.y) <= 50 && Math.abs(pos.x - icespikePos.x) <= 50){
            hp--;
            this.setPosition(new cc.p(pos.x , 250));
        }
    
    },
    
    
    
    eatPotion : function(){
        if(potionExist == true){
            var pos = this.getPosition();
            var potionPos = this.GameLayer.potion.getPosition();
            if(Math.abs(pos.y - potionPos.y) <= 20 && Math.abs(pos.x - potionPos.x) <= 20){
                isEatPotion = true;
                potionExist = false;
                this.GameLayer.potion.removeFromParent(true);
            }
            if(isEatPotion){
                hp++;
                cc.audioEngine.playEffect(res.PotionConsume);
                isEatPotion = false;
            }
        }
    }
    
    
    
});

Character.DIR = [];