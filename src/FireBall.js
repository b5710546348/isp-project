var FireBall = cc.Sprite.extend({
    ctor: function( gm , enemies){
        this._super();
        this.initWithFile(res.FireBalll);
        this.scheduleUpdate();
        this.speed = 5;
        this.enemySet = enemies;
        this.GameLayer = gm;
        
    },
    
    update : function(){
        this.FireBallMovement();
        this.isAtEdge();
        this.isAttacked();
       
    },
    
    
    FireBallMovement : function(){
        var pos = this.getPosition();
        pos.y += this.speed;
        this.setPosition(new cc.p(pos.x , pos.y));
    },
    
    isAtEdge : function(){
        var pos = this.getPosition();
        var self = this;
        if(pos.y >= screenHeight){
            var fireball_fadeout = cc.FadeOut.create(2);
            self.runAction(fireball_fadeout);
            self.removeFromParent(true);
        }
    },
    
    
     
    isAttacked : function(){
        var fireballPos = this.getPosition();
        for(var i = 0 ; i < this.enemySet.length ; i++){
            var enemyPos = this.enemySet[i].getPosition();
            if(this.checkIsHit(fireballPos , enemyPos)){
                score++;
                this.enemySet[i].death();
                this.setPosition(new cc.p(-2000,-2000));
                this.removeFromParent();
                
            }
        }
    },
    
    checkIsHit : function(fireballPos , enemyPos){
         return Math.abs(fireballPos.y - enemyPos.y) <= 20 && Math.abs(fireballPos.x - enemyPos.x) <= 40 ;
    }
	
    
    
    
});