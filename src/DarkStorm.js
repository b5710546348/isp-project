var DarkStorm = cc.Sprite.extend({
	ctor:function (enemy,gm) {
        this._super();
        this.enemy = enemy;
        this.GameLayer = gm;
        
        this.initWithFile('res/dark_strike/darkstrike_1.png');
        var animation = new cc.Animation.create();
        for(var i=1 ; i < 6 ; i++){
            var name = 'res/dark_strike/darkstrike_'+i+'.png';
            animation.addSpriteFrameWithFile( name );
        }
        animation.setDelayPerUnit( 0.1 );
        var movingAction = cc.RepeatForever.create( cc.Animate.create( animation ) );
	    this.runAction( movingAction );
        
        var pos = this.enemy.getPosition();
        this.setPosition(new cc.p(pos.x , pos.y -30));
        this.scheduleUpdate();
    },
    
    update : function (){
        this.isAtEdge();
        this.darkStormMovement();
        this.isHitCharacter();
    },
    
    darkStormMovement : function (){
        var pos = this.getPosition();
        if(pos.y > 0){
            pos.y -= 5;   
        }
        this.setPosition(new cc.p(pos.x , pos.y ));
    },
    
    isHitCharacter : function(){
        var pos = this.getPosition();
        var charPos = this.GameLayer.character.getPosition();
        if(Math.abs(pos.y - charPos.y) <= 20 && Math.abs(pos.x - charPos.x) <= 20){
            hp--;
            this.removeFromParent();
        }
    },
    
    isAtEdge : function () {
        var pos = this.getPosition();
        if(pos.y <= 0){
            this.setPosition(new cc.p(-3000,-5000));
            this.removeFromParent();   
        }
    }
});