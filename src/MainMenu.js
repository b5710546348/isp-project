var MainMenu = cc.Layer.extend({
    ctor : function() {
        this._super();   
        this.init();
    },

    init : function(){
        this._super();   
        var bg = cc.Sprite.create("res/bg.jpg");
        bg.setPosition(new cc.p(screenWidth/2 , screenHeight/2));
        this.addChild(bg);
        this.scheduleUpdate();
        this.addKeyboardHandlers();
        cc.audioEngine.playMusic('res/MainScreen-music.mp3',true);
        return true;
    },
   
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    
    onKeyDown: function( keyCode, event ) {
        
    },
    
    
    onKeyUp: function( keyCode, event ) {
        if (keyCode == cc.KEY.enter) {
            cc.audioEngine.stopMusic( 'res/MainScreen-music.mp3' );
             score = 0;
             num_enemy = 0 ;
             hp = 5;
             numberFireball = 0;
             number_potion = 0;
             max_enemy = 5;
             potionExist = false;
             musicIsStart = false;
            cc.director.runScene(new StartScene());
        }
    },
    
    update: function(dt) {

    }
    
});

var MainScene = cc.Scene.extend({
   onEnter: function() {
       this._super();
       var layer = new MainMenu();
       layer.init();
       this.addChild( layer );
   }
});
    
    
