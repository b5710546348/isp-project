var monsterAttack = false;
var Enemy = cc.Sprite.extend({
    ctor : function(gm,x){
        this._super();
        this.hp = 2;
        //create ghost animation
        this.initWithFile('res/ghost/ghost_1.png');
        var animation = new cc.Animation.create();
        for(var i=1 ; i < 5 ; i++){
            var name = 'res/ghost/ghost_'+i+'.png';
            animation.addSpriteFrameWithFile( name );
        }
        animation.setDelayPerUnit( 0.25 );
        var movingAction = cc.RepeatForever.create( cc.Animate.create( animation ) );
	    this.runAction( movingAction );
        
        //set position
        this.setPosition(new cc.p(x  , screenHeight -50));
        this.GameLayer = gm;
        this.scheduleUpdate();
        
    },
    
    update : function () {
        
        this.DarkStorm();
        
    },
    
    enemyMovement : function () {
        var pos = this.getPosition();
        var notAtEdge = (pos.x >= 50) && (pos.x <= screenWidth - 50) ;
        if(notAtEdge ){
            this.setPosition(new cc.p(pos.x + 1 , pos.y ));
        }
       
    },

    DarkStorm : function (){
        if(Math.floor(Math.random()*1000) % 444 == 0){
            this.darkstorm = new DarkStorm(this,this.GameLayer);
            this.GameLayer.addChild(this.darkstorm);
        }
        
        
    },
    
    death : function() {
        var animation = new cc.Animation.create();
        var pos = this.getPosition();
        for(var i=1 ; i < 5 ; i++){
            var name = 'res/ghost_die/ghost_die_'+i+'.png';
            animation.addSpriteFrameWithFile( name );
        }
        animation.setDelayPerUnit( 0.25 );
        var movingAction = cc.RepeatForever.create( cc.Animate.create( animation ) );
	    this.runAction( movingAction );
        this.setPosition(new cc.p(-3000,-3000));
        this.removeFromParent();
        score++;
        num_enemy--;
    }
   
});
Enemy.size = { width : 80 , length : 100 };


